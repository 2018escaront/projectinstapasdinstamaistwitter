import twitter_image.image_collect as img_coll

def test_image_collect ():

    liste_url = img_coll.collect_image_url('chien')

    print (liste_url)
    #la liste n'est pas vide
    assert len(liste_url) != 0

    #la liste contient bien des url
    assert 'http://' in liste_url
