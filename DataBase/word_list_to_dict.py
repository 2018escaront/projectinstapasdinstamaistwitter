from textblob import TextBlob

def word_list_to_dict(word_list):
    """
    trie les noms et les adjectifs
    :param word_list: liste de mots-clés
    :return: dictionnaire keys:"nom" et "adj"
    """
    nom = []
    adjectif = []
    for word in word_list:
        word_textblob = TextBlob(word)
        if 'JJ' in word_textblob.tags[0][1]:
            adjectif.append(word)
        elif word_textblob.tags[0][1] == 'NN':
            nom.append(word)

    return {'nom':nom, 'adj':adjectif}
