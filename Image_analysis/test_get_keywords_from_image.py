url_image_chat = "http://armand-juliette.a.r.pic.centerblog.net/o/7233fd74.jpg"
url_image_creuse = "http://www.arizona811.com/wp-content/uploads/2016/04/Digging-a-hole_Istock_4.25.16_Resized.jpg"

import Image_analysis.get_keywords_from_image as myFile

def test_get_relevant_tags():
    #les mots attendus sont bien rendus par la fonction
    assert 'cat' in myFile.get_relevant_tags(url_image_chat)
    assert 'fur' in myFile.get_relevant_tags(url_image_chat)
    assert 'soil' in myFile.get_relevant_tags(url_image_creuse)
    #la fonction ne renvoie pas tout et n'importe quoi
    assert 'raton laveur' not in myFile.get_relevant_tags(url_image_creuse)

