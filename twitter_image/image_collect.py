from Twitter_connection.twitter_connection_setup import twitter_setup
import DataBase.Open_phrases as file_open


def collect_tweets(search_term):
    """
    The function takes a search term (string) and returns the related tweets in a list
    """
    print('Collecting the tweets related to {}'.format(search_term))
    connexion = twitter_setup()
    tweets = connexion.search(search_term, count=100)
    return tweets


def collect_image_url(search_terms):
    '''
    récupère des url à partir de search terms
    :param search_terms: list de strings de mots clés
    :return: une liste de url de photos
    '''
    media_files = set() #un set qui contiendra les url
    for term in search_terms:
        tweets = collect_tweets(term)
        for status in tweets:
            media = status.entities.get('media', [])
            if(len(media) > 0) and (len(media_files) < 1): #on récupère si il y a une photo dans le status et si on n'en a pas encore
                media_files.add(media[0]['media_url'])
    if media_files == []: #Si on n'a rien trouvé on recommence mais avec les mots clés du fichier keyword
        search_terms = file_open.Generateur_de_phrases("key")
        for term in search_terms:
            tweets = collect_tweets(term)
            for status in tweets:
                media = status.entities.get('media', [])
                if (len(media) > 0) and (len(media_files) < 1):
                    media_files.add(media[0]['media_url'])
    return list(media_files)
