from DataBase.Open_phrases import Generateur_de_phrases
from random import *
import py.test

def insertion_mots (dict_mots_descr, file_path = './phrases/'):
    '''
    Fonction qui choisit un type de mot (nom, adj) au hasard et qui le place dans une phrase correspondante au hasard
    :param dict_mots_descr (dictionnaire): dictionnaire contenant les mots descriptifs de l'image
    :return phrase_complete (str): phrase complétée avec un mot decrivant l'image
    '''
    type_nom = list(dict_mots_descr.keys())[0]   #type correspondant à la clé d'indice type_ind
    type_adj = list(dict_mots_descr.keys())[1]

    list_nom = dict_mots_descr[type_nom]
    list_adj = dict_mots_descr[type_adj]

    if len(list_nom) > 0 and len(list_adj) > 0:
        #Si on dispose d'un adj et d'un nom on fait une phrase mix

        #choix d'un nom random
        nom_ind = randint(0,len(list_nom)-1)
        nom_a_placer = list_nom[nom_ind]

        #choix d'un adj random
        adj_ind = randint(0,len(list_adj)-1)
        adj_a_placer = list_nom[adj_ind]

        #choix d'une phrase portant nom et adj
        Liste_phrases = Generateur_de_phrases("mix", file_path)
        phrase_ind = randint(0,len(Liste_phrases)-1)
        phrase_a_trou = Liste_phrases[phrase_ind]

        #completion de la phrase choisie avec les mots choisis
        phrase_completee = phrase_a_trou.format(nom_a_placer, adj_a_placer)
    elif len(list_nom) > 0:
        #Si on dispose d'un nom on fait une phrase avec un nom

        #choix d'un nom random
        nom_ind = randint(0,len(list_nom)-1)
        nom_a_placer = list_nom[nom_ind]

        #choix d'une phrase portant nom
        Liste_phrases = Generateur_de_phrases("nom", file_path)
        phrase_ind = randint(0,len(Liste_phrases)-1)
        phrase_a_trou = Liste_phrases[phrase_ind]

        #completion de la phrase choisie avec les mots choisis
        phrase_completee = phrase_a_trou.format(nom_a_placer)
    elif len(list_adj) > 0:
        #Si on dispose d'un adj on fait une phrase avec un adj

        #choix d'un adj random
        adj_ind = randint(0,len(list_adj)-1)
        adj_a_placer = list_nom[adj_ind]

        #choix d'une phrase portant nom et adj
        Liste_phrases = Generateur_de_phrases("mix", file_path)
        phrase_ind = randint(0,len(Liste_phrases)-1)
        phrase_a_trou = Liste_phrases[phrase_ind]

        #completion de la phrase choisie avec les mots choisis
        phrase_completee = phrase_a_trou.format(adj_a_placer)
    else:
        return "Pas de mots descriptifs"

    return phrase_completee



def insertion_hashtags (dict_mots_descr, file_path = './phrases/'):
    '''
    fonction qui renvoie une chaine de caractères sous la forme de hashtags :
    "#mot1   #mot2 ..."
    :param dict_mots_descr (dictionnaire): dictionnaire contenant les mots descriptifs de l'image
    :return ligne_hashtags (str): ligne de texte contenant 5 hashtags
    '''

    Noms = set()
    Adj = set()
    try:    #On teste au cas où le dict_mot_descr ne contient rien dans la clé "nom"
        nb_noms = randint(0, min(5,len(dict_mots_descr['nom'])-1 ) )
        for k in range(nb_noms) :
            Noms.add(dict_mots_descr['nom'][randint(0, len(dict_mots_descr['nom'])-1)])
    except:
        nb_noms = 0
        Noms={"noName"}
    try:    #On teste au cas où le dict_mot_descr ne contient rien dans la clé "nom"
        nb_adj = min(5 - nb_noms, len(dict_mots_descr['adj'])-1 )
        for k in range(nb_adj) :
            Adj.add(dict_mots_descr['adj'][randint(0, len(dict_mots_descr['adj'])-1)])
    except:
        Adj = {"Unadjectivable"}

    ligne_hashtags = ''

    for mot in Noms :
        ligne_hashtags += '#' + mot.replace(' ','') + ' '
    for mot in Adj :
        ligne_hashtags += '#' + mot.replace(' ','') + ' '

    return ligne_hashtags


