import os

def Generateur_de_phrases(Type, file_path = './phrases/'):  #on rajoute un argument pour les tests
    """
    Récupère les phrases de référence stockées dans le fichier file_path/DB_phrases
    :param Type: le type de phrases que l'on veut selon le mot que l'on veut y insérer -> soit 'nom', soit 'adj', soit 'v', soit 'key', soit 'mix'
    :param file_path: le chemin du fichier de stockage des phrases
    :return: (listes) 1 liste contenant des phrases préenregistrées -> 1 de noms, 1 d'adjectifs, 1 de verbes (au chix dans les paramètres)
    """

    #nom des fichiers contenant les phrases
    file_name_Noms = 'Noms.txt'
    file_name_Adjectifs = 'Adjectifs.txt'
    file_name_Keywords = 'Keywords.txt'
    file_name_Mix = 'Mix.txt'

    #création des chemins des fichiers contenant les phrases
    path_Noms = os.path.join(file_path, file_name_Noms)
    path_Adjectifs = os.path.join(file_path, file_name_Adjectifs)
    path_Keywords = os.path.join(file_path, file_name_Keywords)
    path_Mix = os.path.join(file_path, file_name_Mix)

    #ouverture des fichiers et création de listes de phrases (noms, adjectifs, keywords, verbes, mix)
    try:
        ListeNoms = open(path_Noms).readlines()
        ListeAdjectifs = open(path_Adjectifs).readlines()
        ListeKeywords = open(path_Keywords).readlines()
        ListeMix = open(path_Mix).readlines()

        if Type == 'nom':
            return ListeNoms
        if Type == 'adj':
            return ListeAdjectifs
        if Type == 'key':
            return ListeKeywords
        if Type == 'mix':
            return ListeMix #Phrases avec un format 1 et 2 où 1 concerne un nom et 2 un adj


    except IOError:
        print("Le fichier ne peut pas être ouvert, merci de vérifier le chemin d'accès ")


def Generateur_de_mots(Type, file_path = './mots/'):  #on rajoute un argument pour les tests
    """
    Récupère les phrases de référence stockées dans le fichier file_path/DB_phrases
    :param Type: le type de phrases que l'on veut selon le mot que l'on veut y insérer -> ('v','compl_lieu','COD','sujet','exclamation'
    :param file_path: le chemin du fichier de stockage des phrases
    :return: (listes) 1 liste contenant des phrases préenregistrées -> 1 de noms, 1 d'adjectifs, 1 de verbes (au chix dans les paramètres)
    """

    #nom des fichiers contenant les mots
    file_name_Verbes = 'Verbes.txt'
    file_name_Compl_lieu = 'Compl_lieu.txt'
    file_name_COD = 'COD.txt'
    file_name_Sujet = 'Sujet.txt'
    file_name_Exclamation = 'Exclamation.txt'

    #création des chemins des fichiers contenant les phrases
    path_Verbes = os.path.join(file_path, file_name_Verbes)
    path_Compl_lieu = os.path.join(file_path, file_name_Compl_lieu)
    path_COD= os.path.join(file_path, file_name_COD)
    path_Sujet = os.path.join(file_path, file_name_Sujet)
    path_Exclamation = os.path.join(file_path, file_name_Exclamation)

    #ouverture des fichiers et création de listes de phrases (noms, adjectifs, keywords, verbes, mix)
    try:
        ListeVerbes = open(path_Verbes).readlines()
        ListeCompl_lieu = open(path_Compl_lieu).readlines()
        ListeCOD = open(path_COD).readlines()
        ListeSujet = open(path_Sujet).readlines()
        ListeExclamation = open(path_Exclamation).readlines()


        if Type == 'v':
            return ListeVerbes
        if Type == 'compl_lieu':
            return ListeCompl_lieu
        if Type == 'COD':
            return ListeCOD
        if Type == 'sujet':
            return ListeSujet
        if Type == 'exclamation':
            return ListeExclamation

    except IOError:
        print("Le fichier ne peut pas être ouvert, merci de vérifier le chemin d'accès ")
