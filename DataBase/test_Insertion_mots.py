import DataBase.Insertion_mots as Ins

d = {'nom' : ['prout','vagin', 'cacatoes', 'mymy mathy', 'Ernest Chombier' ], 'adj' : ['moche', 'chaud', 'bandant', 'clownesque', 'fermé', 'énorme']}

def test_insertion_mots ():
    #Le return est bien une phrase
    assert type(Ins.insertion_mots(d)) == str

    #Au moins 1 des mots de la liste est dans la phrase
    bool = False
    phrase = Ins.insertion_mots(d)
    for nom in d['nom'] :
        if nom in phrase :
            bool = True
    for adj in d['adj'] :
        if adj in phrase :
            bool = True
    assert (bool)

def test_insertion_hashtags () :

    #le return contient bien des hashtags
    hashtags = Ins.insertion_hashtags(d)
    for i in range (len(hashtags)) :
        if hashtags[i]=='#' and i != 0:
            assert hashtags[i-1]==' '


    #les hashtags sont bien dans la liste :
    k=0
    mots=[]
    for i in range (len(hashtags)):
        if hashtags[i] == '#':
            if hashtags[k]!=' ' or k<=len(hashtags-1):
                k+=1
            else :
                mots += hashtags[i+1:k]
                k+=1
    for mot in mots :
        assert mot in d['nom'] or mot in d['adj']



