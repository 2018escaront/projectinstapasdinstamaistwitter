import DataBase.Open_phrases as myFile

def test_Generateur_de_phrases():
    assert len(myFile.Generateur_de_phrases("nom")) >= 1
    assert len(myFile.Generateur_de_phrases("adj")) >= 1
    assert len(myFile.Generateur_de_phrases("mix")) >= 1
    assert len(myFile.Generateur_de_mots("v")) >= 1
    assert len(myFile.Generateur_de_mots("exclamation")) >= 1
    assert len(myFile.Generateur_de_mots("compl_lieu")) >= 1
    assert len(myFile.Generateur_de_mots("COD")) >= 1
    assert len(myFile.Generateur_de_mots("sujet")) >= 1

