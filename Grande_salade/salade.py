import Image_analysis.get_keywords_from_image as file_get
import twitter_image.image_collect as file_image
import DataBase.Insertion_mots as file_insertion
import DataBase.word_list_to_dict as file_word
import DataBase.assemble_phrases as file_assemble

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import requests

def show_tweet_from_search_term(search_term):
    """
    collecte des images Ã  partir d'un mot clÃ© et renvoie un tweet par image qui parle de l'image
    :param search_term: string d'un terme de recherche pour trouver des images sur twitter
    :return tweets: renvoie une liste de textes de tweets
    """
    list_urls = file_image.collect_image_url([search_term])
    tweets = []
    for url in list_urls:
        tweet = create_tweet_from_url(url)
        tweets.append((tweet,url))
        Open_image(url,tweet)
    return tweets


def Open_image(url, text):
    """
    fonction pour afficher un tweet
    :param url: url de l'image a  afficher
    :param text: text du tweet a  afficher
    :return: affiche un tweet complet
    """

    #telechargement de l'image à partir de l'url
    r = requests.get(url, allow_redirects=True)
    open('image.jpg', 'wb').write(r.content)

    image = Image.open('image.jpg')
    image_np = np.asarray(image)
    plt.axis('off') #enlever les axes
    plt.imshow(image_np)
    plt.title(text) #appliquer un texte à l'image
    plt.show()

def create_tweet_from_url(url):
    """
    prend un url d'image pour en faire un texte de tweet qui en parle
    :param url: url d'une image
    :return text: le text d'un tweet avec les Ã©lÃ©ments de l'image
    """
    keywords = file_get.get_relevant_tags(url)
    dict_keywords = file_word.word_list_to_dict(keywords)
    text = file_insertion.insertion_mots(dict_keywords,file_path="../DataBase/phrases/")+file_insertion.insertion_hashtags(dict_keywords,file_path="../DataBase/phrases/")
    text = file_assemble.assemble_phrases(dict_keywords,file_path="../DataBase/mots/")+file_insertion.insertion_hashtags(dict_keywords,file_path="../DataBase/phrases/")
    return text

