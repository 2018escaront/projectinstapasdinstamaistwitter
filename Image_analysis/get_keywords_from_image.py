

from Image_analysis.clarify_api_keys import *
from clarifai.rest import ClarifaiApp

app = ClarifaiApp(api_key = KEY_ANALYSE_OF_INSTA_IMAGE)
model = app.models.get("general-v1.3")


def get_relevant_tags(image_url):
    """
    :param image_url: url de l'image à diagnostiquer
    :return: liste de concepts associés à l'image en anglais deja tries par ordre d'importance
    """
    response_data = app.tag_urls([image_url])

    tag_urls = []
    for concept in response_data['outputs'][0]['data']['concepts']:
        tag_urls.append( concept['name']) # , concept['value'] ) ) #pour renvoyer une liste de couples nom,value

    return tag_urls


