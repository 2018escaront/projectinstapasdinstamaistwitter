import DataBase.Open_phrases as opener
from random import randint

def assemble_phrases(dict_mots_descr, file_path = './mots/'):
    type_nom = list(dict_mots_descr.keys())[0]   #type correspondant  au nom
    type_adj = list(dict_mots_descr.keys())[1]  #type correspondant  a un adj

    list_nom = dict_mots_descr[type_nom]
    list_adj = dict_mots_descr[type_adj]

    if list_adj == []:
        list_adj = opener.Generateur_de_mots("adj",file_path)
    if list_nom == []:
        list_nom = opener.Generateur_de_mots("sujet",file_path)
    print(list_adj)
    list_cod = opener.Generateur_de_mots("COD",file_path)
    list_exclamation = opener.Generateur_de_mots("exclamation",file_path)
    list_compl_lieu = opener.Generateur_de_mots("compl_lieu",file_path)
    list_verb = opener.Generateur_de_mots("v",file_path)

    ind_nom = randint(0,len(list_nom)-1)
    ind_adj = randint(0,len(list_adj)-1)
    ind_cod = randint(0,len(list_cod)-1)
    ind_exclamation = randint(0,len(list_exclamation)-1)
    ind_compl_lieu = randint(0,len(list_compl_lieu)-1)
    ind_verb = randint(0,len(list_verb)-1)

    word_nom = list_nom[ind_nom].strip()
    word_adj = list_adj[ind_adj].strip()
    word_cod = list_cod[ind_cod].strip()
    word_exclamation = list_exclamation[ind_exclamation].strip()
    word_compl_lieu = list_compl_lieu[ind_compl_lieu].strip()
    word_verb = list_verb[ind_verb].strip()

    phrase_assemblee = "oooh non pas de phrase..."

    #traitement du word_verb

    def traite_word_verb(vb):
        vb = vb.replace("(lieu,adv)","")
        vb = vb.replace("(adj)","")
        vb = vb.replace("(comp)","")
        vb = vb.replace("(lieu)","")
        return vb

    if 'adj' in word_verb:
        word_verb   = traite_word_verb(word_verb)
        phrase_assemblee = word_exclamation+" "+word_nom+" "+word_verb+" "+word_adj+" \n "
    if 'lieu' in word_verb:
        word_verb   = traite_word_verb(word_verb)
        phrase_assemblee = word_exclamation+" "+word_nom+" "+word_verb+" "+word_compl_lieu+" \n"
    if 'comp' in word_verb:
        word_verb   = traite_word_verb(word_verb)
        phrase_assemblee = word_exclamation+" "+word_nom+" "+word_verb+" "+word_cod+" \n"

    return phrase_assemblee
